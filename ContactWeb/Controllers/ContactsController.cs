﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ContactWeb.Models;

namespace ContactWeb.Controllers
{
    public class ContactsController : Controller
    {

        public ActionResult Index()
        {
            ViewData["Title"] = "List of available contacts";
            ViewBag.CrudAction = "Add";

            var contacts = _DbContext.Contacts;

            return View(contacts);

        }

        public ActionResult Add()
        {
            PrepFormControls();
            ViewData["Title"] = "Adding a contact";
            ViewBag.CrudAction = "Create";
            var contact = new ContactWeb.Models.Contact();
            contact.Birthday = DateTime.Now.AddYears(-21);
            contact.UserId = Guid.NewGuid();
            return View("Edit", contact);

        }

        public ActionResult Edit(int id)
        {
            PrepFormControls();
            ViewData["Title"] = "Editing a contact";
            ViewBag.CrudAction = "Update";

            var contacts = _DbContext.Contacts;
            var contact = contacts.First(m => m.Id == id);


            return View(contact);

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Contact contact)
        {
            ViewData["Title"] = "Adding a Service Type";
            if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", contact); }

            var contacts = _DbContext.Contacts;
            contacts.Add(contact);
            await _DbContext.SaveChangesAsync();

            return RedirectToAction(GetIndexAction());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(ContactWeb.Models.Contact contact, string updateType)
        {
            ViewData["Title"] = "Editing a contact";

            var contacts = _DbContext.Contacts;

            if (updateType.Equals("Delete"))
            {
                _DbContext.Entry(contact).State = EntityState.Deleted;
                await _DbContext.SaveChangesAsync();
            }
            else
            {
                if (!ModelState.IsValid) { PrepFormControls(); return View("Edit", contact); }
                _DbContext.Entry(contact).State = EntityState.Modified;
                await _DbContext.SaveChangesAsync();
            }
            return RedirectToAction(GetIndexAction());

        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters;
            ViewBag.States = new List<string> {"FL", "AL", "NY", "GA" };

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);
        }

        private readonly ApplicationDbContext _DbContext;
        public ContactsController() { _DbContext = new ApplicationDbContext(); } // ctor
        protected override void Dispose(bool disposing) { _DbContext.Dispose(); }

    }
}
