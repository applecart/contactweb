﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "CRUD templates reduce TCO and end user training.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "djacob@applecart.net";

            return View();
        }
    }
}