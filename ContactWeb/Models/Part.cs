﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactWeb.Models
{
    public class Part
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}